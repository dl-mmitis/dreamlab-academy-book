import React from 'react';
import ReactDOM from 'react-dom';

import BookApp from './components/BookApp';

ReactDOM.render(<BookApp> </BookApp>, document.getElementById('root'));