import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';

class SearchInput extends React.Component {

    constructor(props){
        super(props);
        this.delayedCallback = _.debounce(() => {
                this.props.onChange(this.inputRef.value);
        }, 500);
        this.onChangeHandler = this.onChangeHandler.bind(this);
    }

    onChangeHandler(event){
        event.persist();
        this.delayedCallback(event);
    }

    render(){
        return (
            <div className="form-group">
                <input ref={(r) => {this.inputRef = r }} className="form-control"
                 onChange={this.onChangeHandler} />
            </div>
        );
    }
}

SearchInput.propTypes = {
    onChange: PropTypes.func.isRequired
};

export default SearchInput;