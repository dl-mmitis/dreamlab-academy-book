import React from 'react';
import SearchInput from './SeachInput';
import Book from './Book';
import BookAPIFetcher from './../services/BookAPIFetcher';

class BookApp extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            books: [],
            searchPhrase: '' 
        };
        this.updateSearch = this.updateSearch.bind(this);
    }

    performSearch(){
        BookAPIFetcher.searchBooks(this.state.searchPhrase)
            .then((books) => {
                this.setState({
                    books   
                });
            })
    }

    updateSearch(value){
        this.setState({
            searchPhrase : value   
        }, () => {
            this.performSearch();
        });
    }

    render(){
        return (<div>
            <SearchInput onChange={this.updateSearch} />
            {JSON.stringify(this.state.books)}
            {this.state.books.map((bookData) => {
                return <Book book={bookData} />
            })}
        
        </div>);
    }
}

export default BookApp;