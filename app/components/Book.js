import React from 'react';

const Book = ({ book }) => {
    return (
        <div className='list-group-item card'>
            <img src={book.image} 
                style={{float: 'left', width: '100px', height: 200}}
                alt={book.title} />
            <div className='card-block'>
                <h4 className='card-title'>{book.title}</h4>
                <p className='card-description'>
                    {book.description}
                </p>
            </div>
        </div>
    )
}

Book.propTypes = {

}

export default Book;